package config

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfigParse(t *testing.T) {
	asrt := assert.New(t)

	rawCfg := []byte(`
        {
            "server": {
                "endpoint": ":8080"
            },
            "debugserver": {
                "endpoint": "localhost:9000"
            }
        }
    `)
	cfg, err := Parse(bytes.NewReader(rawCfg))
	if !asrt.NoError(err) {
		t.Fatalf("unable to parse raw config %v", err)
	}

	asrt.Equal(":8080", cfg.Server.Endpoint)
	asrt.Equal("localhost:9000", cfg.DebugServer.Endpoint)
}
