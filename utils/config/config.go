package config

import (
	"encoding/json"
	"io"
)

// Plugin is a special configuration type for an emailsender plugin
type Plugin map[string]interface{}

// Logging is configuration for logging system
type Logging struct {
	Level  string `json:"level"`
	Output string `json:"output"`
}

// Server is a section about HTTP Server to handle users' requests
type Server struct {
	Endpoint string `json:"endpoint"`
}

// DebugServer is a section about debug server for httpprof and metrics
type DebugServer struct {
	Endpoint string `json:"endpoint"`
}

// General describes the whole configuration file
type General struct {
	Logging     `json:"logging"`
	Server      `json:"server"`
	DebugServer `json:"debugserver"`

	Senders map[string]Plugin `json:"senders"`
}

// Parse decodes configuration from io.Reader
func Parse(r io.Reader) (*General, error) {
	var cfg General
	if err := json.NewDecoder(r).Decode(&cfg); err != nil {
		return nil, err
	}

	return &cfg, nil
}
