[![CircleCI](https://circleci.com/bb/noxiouz/ucchallenge.svg?style=svg)](https://circleci.com/bb/noxiouz/ucchallenge)

# Description

Create a service that accepts the necessary information and sends emails. It should provide an abstraction between two different email service providers. If one of the services goes down, your service can quickly failover to a different provider without affecting your customers.

It's backend focused solution. As the task is not so widely described, I prefer to spend more time on infrastructure critical aspects and to show my point of view on:
 * logs
 * metrics
 * configuration

I assume that *nginx* terminates HTTPS traffic, because it's fast, reliable, secure(!) and is flexible to configure.
There is no rate limiting system in the project because it makes no sense without authentication.

# Architecture

There are several packages:
 * plugin
 * server
 * emailsender
 * utility packages

`server` just wraps a library to provide HTTTP-interface.

`emailsender` defines `Sender` interface. It's an abstraction around all email providers. The interface must be implemented by plugins. Also this package
provides registry for registering plugins. So when `server` creates plugins, it uses registry interface, not direct access to constructors.

Each `plugin` implements one of email providers. `Amazon SES`, `Sendgrid` are implemented and tested. `Mailgun` is implemented, but not tested.

Distribution algorithm is very simple. Random permutation generated and is used as order in which backends are used in the current request.
At the first glance it's too simple. But if we want to use dynamic weights, we have to respect such facts as cost of every email,
rate limitation (only Sendgrid provides information about limit, AFAIK), time to sent and many others. It's a big research.
So for the beta simple algorithm fits good. Also it takes on account that email providers are very reliable, so it's really small
possibility to have more than one of them down. And most of errors will be related to rate limits, in this case simple algorithm works well.

# How to build

go 1.7.x required (1.7.4 is recommended due to some security fixes)

```bash
make build
```

This command produces `ucchallenge` executable in the root of the repo.

# How to try

It's deployed on a Scaleway server

```bash
curl  -F subject="subject" -F message="message" -F name="somename" -F email="mail@yandex.ru" http://212.47.241.168:8080/v1/message
```

# What I might do differently if I were to spend additional time

Also I would spend more time on finding other client libraries for email services or patching them to improve in following aspects:
 * not all of them allows to redefine destination of service. It makes hard to mock service and also **eats money** :). For example `Sendgrid` provides a way to rewrite API destination, which allows to test rate limit overflow case.
 * not all of them allow to specify HTTPClient, which prevents customization of http.Transport (IPv6, IdleConnections). Also it's impossible to inject tracing system.

I would definitely support OpenTracing. There is a place to attach Trace and context is passing around all calls.

This project has no authorization/authentication mechanism. Even simple solution requires web page to register users, generates tokens. I will do this, but it requires time :(.

There are placeholders in the project where this mechanism can painless inject.
After all these features **load testing** goes. No point to test without auth, good provides mocks (otherwise it costs a lot!!!), tracing (to find actual hot spots and problems).

Two of email provides are not so easy to use. For example, Mailgun suspended my account for business verification.
The last one: CircleCI does not send coverage report to codecov, although the report is generated properly.

After load balancing tests and finding out which metrics can be provided by email Senders more effective algorithm of distribution might be picked.

# Other projects I'm proud of

## Python projects

[cocaine-framework-python](https://github.com/cocaine/cocaine-framework-python)

It's a framework for our open-sourced PaaS, which allows to write applications on python.
The challenge was to write very effective framework for event-driven asynchronous applications with lazy network (re)connection.
This framework uses dynamic properties of Python to create very clear API related to dynamic nature of Cocaine cloud protocol.
It's very well covered by tests, easy to inject into any other `Tornado` (or `asyncio`) based application.
Supports both python `2` and `3`. The framework is a very important part of our cloud infrastructure.
Both deployment tools and HTTP entry point to the cloud are based on the client part of this framework. The only disadvantage is there are no docs :( .
I like this project also because the previous version was my first big project on Python and by comparing the versions I see my progress.

[proxy](https://github.com/cocaine/cocaine-tools/tree/master/cocaine/proxy)

It's proxy based on the framework. Because of python GIL nature I had to spawn processes to utilize CPU effectively in asynchronous IO.
`SO_REUSEPORT` (I made this feature in Tornado after the research) is used here for listening socket in the proxy to more fair distribution of accepts and make graceful restart possible. The idea was to gain as much performance as it's possible from one multicore server, because there were not good HTTP framework for C++.
A lot of work was done here to support context based logging (attached to HTTP request), fingers-crossed logging (log the hole trace only on error) and many more.

## Go projects

[stout](https://github.com/noxiouz/stout)

It's a heavy loaded daemon, core critical component, which rules instances of applications in our cloud. Its architecture based on pluggable system. Each plugin is a different type of process isolation (native process, `Docker` based, `Porto` based).
There were some problems here, which were solved by me:
 * spawning lots of child processes from go program, collecting there output in near realtime manner, preventing zombie rush. `cmd.Wait()` mechanism does not work here, because requires a thread for each wait.
 * in process metrics, which are sent by the daemon to a monitoring system. Also some health checks are implemented to alert monitoring.
 * context-based logging makes easy to track connections and requests (every connection is multiplexed)
 * as sometimes it forks many times per second memory footprint should be minimized.

[elliptics-go](github.com/noxiouz/elliptics-go)

It was many years ago when I needed to implement Go client for Elliptics storage as a wrapper around asynchronous C++ library (without SWIG).
The code quality is not so good (actually it was one of my first go projects), but I'm proud of solutions which allowed to prevent GC collecting of callbacks. Also I found **some bugs in `cgo`** due to this project: [11907](https://github.com/golang/go/issues/11907) [12238](https://github.com/golang/go/issues/12238)
One company is making money on products based on this package (not only it, but still).

[cocaine-framework-go](https://github.com/cocaine/cocaine-framework-go/tree/master/cocaine12)

Go framework for the cloud. The feature is supporting Zipkin-style tracing, which helps to debug many problems in users' applications. That's why I am really a big fan of http://opentracing.io/.
