package main

import (
	"context"
	"expvar"
	"fmt"
	"net/http"
	"os"
	"runtime"
	"runtime/pprof"
	"strings"
	"syscall"
	"time"

	"github.com/noxiouz/stout/pkg/exportmetrics"
	"github.com/noxiouz/stout/pkg/fds"
	"github.com/ogier/pflag"
	"github.com/pborman/uuid"
	metrics "github.com/rcrowley/go-metrics"
	"github.com/uber-go/zap"

	"bitbucket.org/noxiouz/ucchallenge/server"
	"bitbucket.org/noxiouz/ucchallenge/utils/config"
	"bitbucket.org/noxiouz/ucchallenge/utils/log"
	"bitbucket.org/noxiouz/ucchallenge/version"

	_ "bitbucket.org/noxiouz/ucchallenge/emailsender/amazonses"
	_ "bitbucket.org/noxiouz/ucchallenge/emailsender/sendgrid"
)

var (
	flagConfig      string
	flagShowVersion bool

	fmtVersion = fmt.Sprintf("version=%s\tbuild=%s\thash=%s\ttag=%s\n", version.Version, version.Build, version.GitHash, version.GitTag)
)

// TODO: move all metrics out to a package
var (
	// Current amount of opened files must be less the ulimit -n
	// otherwise we can not accept connections
	openFDs    = metrics.NewGauge()
	goroutines = metrics.NewGauge()
	// spawned threads never exit, for example to support pdeathsig mechanism,
	// so we need to control the amount to prevent panic when default limit is reached (10k)
	threads = metrics.NewGauge()
)

func init() {
	pflag.StringVarP(&flagConfig, "config", "c", "", "path to the configuration file")
	pflag.BoolVarP(&flagShowVersion, "version", "v", false, "show version and exit")
	pflag.Parse()

	registry := metrics.NewPrefixedChildRegistry(metrics.DefaultRegistry, "daemon_")
	registry.Register("open_fds", openFDs)
	registry.Register("goroutines", goroutines)
	registry.Register("threads", threads)

	registry.Register("hc_openfd", metrics.NewHealthcheck(fdHealthCheck))
	registry.Register("hc_threads", metrics.NewHealthcheck(threadHealthCheck))

	http.Handle("/metrics", exportmetrics.HTTPExport(metrics.DefaultRegistry))

	expvar.NewString("version_info").Set(fmtVersion)
}

func fdHealthCheck(h metrics.Healthcheck) {
	var l syscall.Rlimit
	if err := syscall.Getrlimit(syscall.RLIMIT_NOFILE, &l); err != nil {
		h.Unhealthy(err)
		return
	}

	// -100 is used as safe gap. When the limit is reached we can not even run this Healthcheck
	if val := openFDs.Value(); uint64(val) >= l.Cur-100 {
		h.Unhealthy(fmt.Errorf("too many open files %d (max %d)", val, l.Cur))
		return
	}

	h.Healthy()
}

func threadHealthCheck(h metrics.Healthcheck) {
	if val := threads.Value(); val >= 5000 {
		h.Unhealthy(fmt.Errorf("too many OS threads %d (max 10000)", val))
		return
	}

	h.Healthy()
}

func collect(ctx context.Context) {
	goroutines.Update(int64(runtime.NumGoroutine()))
	count, err := fds.GetOpenFds()
	if err != nil {
		log.G(ctx).Error("get open fd count", zap.Error(err))
		return
	}

	openFDs.Update(int64(count))
	threads.Update(int64(pprof.Lookup("threadcreate").Count()))

	metrics.DefaultRegistry.RunHealthchecks()
}

func genInstanceID() string {
	// NOTE: also pid info can be included
	hostname, err := os.Hostname()
	if err != nil {
		log.L.Warn("unable to get Hostname", zap.Error(err))
		hostname = uuid.NewRandom().String()
	}

	return hostname
}

func showVersion() {
	fmt.Println(fmtVersion)
}

func main() {
	if flagShowVersion {
		showVersion()
		return
	}

	if flagConfig == "" {
		log.L.Fatal("non-empty path to configuration file must be specified via --config/-c")
	}

	f, err := os.Open(flagConfig)
	if err != nil {
		log.L.Fatal("unable to Open configuration file", zap.Error(err))
	}
	defer f.Close()

	// Parse configuration file
	cfg, err := config.Parse(f)
	if err != nil {
		log.L.Fatal("unable to parse configuration file", zap.Error(err))
	}
	f.Close()

	// Configure logger
	lvl := zap.DynamicLevel()

	outputOption := zap.Output(os.Stderr)
	switch cfg.Logging.Output {
	case "", "/dev/stderr":
	case "/dev/stdout":
		outputOption = zap.Output(os.Stdout)
	default:
		outputFile, err := os.OpenFile(cfg.Logging.Output, os.O_APPEND|os.O_CREATE, 0600)
		if err != nil {
			log.L.Fatal("unable to open logging output", zap.Error(err))
		}
		defer outputFile.Close()
	}

	logger := zap.New(zap.NewTextEncoder(), outputOption, lvl)
	logger = logger.With(zap.String("instance", genInstanceID()))
	// TODO: move this check to configuration by implementing wrapper type for zap.Level
	// with JSON.Unmarshaller interface
	switch strings.ToLower(cfg.Logging.Level) {
	case "debug":
		lvl.SetLevel(zap.DebugLevel)
	case "info":
		lvl.SetLevel(zap.InfoLevel)
	case "warn":
		lvl.SetLevel(zap.WarnLevel)
	case "error":
		lvl.SetLevel(zap.ErrorLevel)
	default:
		lvl.SetLevel(zap.InfoLevel)
		logger.Warn("unknown loglevel provided. Info level is set", zap.String("passed", cfg.Logging.Level))
	}
	ctx := log.WithLogger(context.Background(), logger)

	go func() {
		collect(ctx)
		for range time.Tick(30 * time.Second) {
			collect(ctx)
		}
	}()

	if cfg.Server.Endpoint == "" {
		logger.Fatal("Server endpoint is not specified")
	}

	// Start debug server if configured
	// NOTE: need to check somehow that it's accessable only from localhost
	// The best solution is to start debug server on unix socket to control access via Unix access system,
	// but go tool pprof does not support unix socket AFAIK
	if cfg.DebugServer.Endpoint != "" {
		go func() {
			logger.Info("start debug HTTP-server", zap.String("endpoint", cfg.DebugServer.Endpoint))
			err := http.ListenAndServe(cfg.DebugServer.Endpoint, nil)
			// NOTE: actually it's not always an error
			logger.Error("DebugServer exits", zap.Error(err))
		}()
	}

	srv, err := server.NewHandler(ctx, cfg)
	if err != nil {
		logger.Fatal("unable to create server", zap.Error(err))
	}
	defer srv.Close()

	// TODO: graceful restarted server is better to use here. For example, from go 1.8
	// Socket activation mechanism from systemd can be used.
	// SO_REUSEPORT is not easy to set on listening socket in golang, so systemd is the easiest
	// way pass such socket.
	// TODO: handles signals
	httpSrv := http.Server{
		Addr:           cfg.Server.Endpoint,
		Handler:        srv,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	err = httpSrv.ListenAndServe()
	logger.Fatal("HTTP Server exited", zap.Error(err))
}
