#!/usr/bin/env make

PKG=bitbucket.org/noxiouz/ucchallenge
NAME=ucchallenge
BUILDDT=$(shell date -u +%F@%H:%M:%S)
VERSION=$(shell git show-ref --head --hash head)
TAG=$(shell git describe --tags --always)
DEBVER=$(shell dpkg-parsechangelog | sed -n -e 's/^Version: //p')
LDFLAGS=-ldflags "-X ${PKG}/version.GitTag=${TAG} -X ${PKG}/version.Version=${DEBVER} -X ${PKG}/version.Build=${BUILDDT} -X ${PKG}/version.GitHash=${VERSION}"


.DEFAULT: all
.PHONY: fmt vet test

PKGS := $(shell go list ./... | grep -v ^${PKG}/vendor/ | grep -v ^${PKG}/version)

all: fmt vet test build

vet:
	@echo "+ $@"
	@go vet $(PKGS)

fmt:
	@echo "+ $@"
	@test -z "$$(gofmt -s -l . 2>&1 | grep -v ^vendor/ | tee /dev/stderr)" || \
		(echo >&2 "+ please format Go code with 'gofmt -s'" && false)

lint:
	@echo "+ $@"
	@test -z "$$(golint ./... 2>&1 | grep -v ^vendor/ | tee /dev/stderr)"

test:
	@echo "+ $@"
	@echo "" > coverage.txt
	@set -e; for pkg in $(PKGS); do go test -coverprofile=profile.out -covermode=atomic $$pkg; \
	if [ -f profile.out ]; then \
		cat profile.out >> coverage.txt; rm  profile.out; \
	fi done; \

build: ucchallenge

ucchallenge: cmd/main.go
	@echo "+ $@"
	go build ${LDFLAGS} -o ${NAME} ${PKG}/cmd
