package server

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/pborman/uuid"
	"github.com/uber-go/zap"

	"bitbucket.org/noxiouz/ucchallenge/emailsender"
	"bitbucket.org/noxiouz/ucchallenge/utils/config"
	"bitbucket.org/noxiouz/ucchallenge/utils/log"
)

const (
	subjectKey = "subject"
	messageKey = "message"
	nameToKey  = "name"
	emailToKey = "email"
)

// Handler handles requests and can be closed to deallocate plugins
type Handler interface {
	http.Handler
	Close()
}

type server struct {
	context.Context
	*mux.Router

	balancer *emailsender.Balancer
}

type errorResponse struct {
	Reason string `json:"reason,omitempty"`
}

func (s *server) Close() {
	s.balancer.Close()
}

func (s *server) sendEmail(w http.ResponseWriter, r *http.Request) {
	parseAndCheck := func(key string) string {
		value := r.PostFormValue(key)
		if value == "" {
			log.G(r.Context()).Error("key can not be parsed from POST", zap.String("key", key))
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(errorResponse{
				Reason: fmt.Sprintf("%s parameter is missing", key),
			})
		}
		return value
	}

	subject := parseAndCheck(subjectKey)
	if subject == "" {
		return
	}
	message := parseAndCheck(messageKey)
	if message == "" {
		return
	}
	name := parseAndCheck(nameToKey)
	if name == "" {
		return
	}
	email := parseAndCheck(emailToKey)
	if email == "" {
		return
	}

	m := emailsender.Mail{
		To: emailsender.To{
			Name:  name,
			Email: email,
		},
		Subject: subject,
		Message: message,
	}
	if err := s.balancer.Send(r.Context(), m); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(errorResponse{
			Reason: "unable to send email",
		})
		return
	}

	w.WriteHeader(http.StatusOK)
}

func authWrapper(ctx context.Context, f http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// TODO: any auth system can be injected here

		// NOTE: another strategy is possible here
		// We can get Request ID from some header which is set by nginx (for example via special nginx module, that generates IDs)
		// It can be usefull to track the request in nginx's logs too
		reqID := uuid.NewRandom().String()
		ctx := log.WithLogger(ctx, log.GetLogger(ctx).With(zap.String("id", reqID)))
		r = r.WithContext(ctx)
		w.Header().Set("X-Request-Id", reqID)
		f(w, r)
	}
}

// NewHandler returns http.Handler for users' requests
func NewHandler(ctx context.Context, cfg *config.General) (Handler, error) {
	s := &server{
		Context:  ctx,
		Router:   mux.NewRouter(),
		balancer: emailsender.NewBalancer(),
	}
	s.Router.HandleFunc("/v1/message", authWrapper(s.Context, s.sendEmail)).Methods("POST")

	for name, pluginConfig := range cfg.Senders {
		sender, err := emailsender.NewSender(name, pluginConfig)
		if err != nil {
			log.G(ctx).Error("unable to initialize plugin", zap.String("plugin", name), zap.Error(err))
			s.balancer.Close()
			return nil, err
		}
		s.balancer.Add(name, sender)
	}
	return s, nil
}
