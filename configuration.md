# configuration

```json
{
    "logging": {
        "level": "debug",
        "output": "/dev/stderr"
    },
    "debugserver": {
        "endpoint": "localhost:9000"
    },
    "server": {
        "endpoint": ":8080"
    },
    "senders": {
            "sendgrid": {
                "api_key": "<sendgrid_api_key>",
                "address": "https://api.sendgrid.com",
                "from": {
                    "email": "mypersonalmailbox@mail.com",
                    "user": "noxiouz"
                }
            },
            "amazonses": {
                "accesskeyid": "<AWSAccessKeyId>",
                "secretaccesskey": "<AWSSecretKey>",
                "endpoint": "https://email.us-west-2.amazonaws.com",
                "verifiedemail": "<AWSVerifiedEmail>"
            }
    }
}
```

## logging

This section describes options for logger.

 * `level` is minimal level of messages. Currently `debug`, `info`, `warn`, `error`. Default level is `info`. Dynamic level change is not supported yet.
 * `output` can be `/dev/stderr`, '/dev/stdout' or a path to a file on filesystem. The file is opened with O_CREATE | O_APPEND. Default value "/dev/stderr". Also there is a default global logger which writes logs to `stderr`. It's used until the main logger is configured

## debugserver

Debug server exports metrics, go runtime metrics, endpoints for `pprof` and `tracing`.

 * `endpoint` is where the debug server starts. Strictly recommender not to start it on public address.

## server

Server handles users' requests. HTTPS is not supported yet, because I believe that `nginx` is better for SSL/TLS termination.

 * `endpoint` is where the server starts.

## senders

This section configures email providers.

### sendgrid

 * `api_key` is a secretkey to use Sendgrid api
 * `address` is destination of Sendgrid. If it's empty "https://api.sendgrid.com" will be used
 * `from.email`, `from.user` sets email from which all emails will be sent

### amazonses

 * `accesskeyid` is your Amazon AWS access key ID
 * `secretaccesskey` is your Amazon AWS secret key
 * `endpoint` is the AWS endpoint to use for requests (i.e https://email.us-west-2.amazonaws.com)
 * `verifiedemail` is your verifed email on AWS
