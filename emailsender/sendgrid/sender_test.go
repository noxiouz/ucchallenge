package sendgrid

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"sync/atomic"
	"testing"

	"bitbucket.org/noxiouz/ucchallenge/emailsender"

	"github.com/stretchr/testify/assert"
)

func TestConfigDecoder(t *testing.T) {
	asrt := assert.New(t)

	const (
		key      = "somekey"
		fromUser = "some_user"
		email    = "example@mail.com"
		address  = "https://test.com"
	)
	var cfg config
	rawcfg := map[string]interface{}{
		"api_key": key,
		"from": map[string]string{
			"user":  fromUser,
			"email": email,
		},
	}
	err := decodeConfig(rawcfg, &cfg)
	if !asrt.NoError(err) {
		t.Fatalf("unable to decode config %v", err)
	}
	asrt.Equal(defaultSendgridAddress, cfg.Address)
	asrt.Equal(key, cfg.APIKey)
	asrt.Equal(email, cfg.From.Email)
	asrt.Equal(fromUser, cfg.From.User)

	rawcfg["address"] = address
	err = decodeConfig(rawcfg, &cfg)
	if !asrt.NoError(err) {
		t.Fatalf("unable to decode config %v", err)
	}
	asrt.Equal(address, cfg.Address)
}

func TestSendgridSend(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}
	// NOTE: this test eats your money!
	asrt := assert.New(t)
	// NOTE: go1.8b provides test context in testing.T
	ctx := context.Background()

	rawcfg := map[string]interface{}{
		"api_key": os.Getenv("SENDGRID_KEY"),
		"from": map[string]string{
			"user":  "noxiouz",
			"email": "noxiouz@yandex.ru",
		},
	}
	s, err := newSender(rawcfg)
	if !asrt.NoError(err) {
		t.Fatalf("unable to create Sendgrid client")
	}
	defer s.Close()

	m := emailsender.Mail{
		Message: "test message from Noxiouz",
		Subject: "test message from noxiouz",
		To: emailsender.To{
			Name:  "noxiouz",
			Email: "noxiouz@yandex.ru",
		},
	}
	err = s.Send(ctx, m)
	asrt.NoError(err)
}

func TestSendgridSendErrors(t *testing.T) {
	asrt := assert.New(t)
	// NOTE: go1.8b provides test context in testing.T
	ctx := context.Background()

	var i uint64
	const limit = 10

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if res := atomic.AddUint64(&i, 1); res == limit {
			w.WriteHeader(http.StatusTooManyRequests)
		}
		fmt.Fprintln(w, "Hello, client")
	}))
	defer ts.Close()

	rawcfg := map[string]interface{}{
		"api_key": "some_key",
		"address": ts.URL,
		"from": map[string]string{
			"user":  "noxiouz",
			"email": "noxiouz@yandex.ru",
		},
	}

	s, err := newSender(rawcfg)
	if !asrt.NoError(err) {
		t.Fatalf("unable to create Sendgrid client")
	}
	defer s.Close()

	m := emailsender.Mail{
		Message: "test message from Noxiouz",
		Subject: "test message from noxiouz",
		To: emailsender.To{
			Name:  "noxiouz",
			Email: "noxiouz@yandex.ru",
		},
	}
	for j := 0; j < limit-1; j++ {
		err = s.Send(ctx, m)
		asrt.NoError(err)
	}

	err = s.Send(ctx, m)
	terr, ok := err.(emailsender.Error)
	if !ok {
		t.Fatalf("error is not emailsender.Error %v %T", err, err)
	}
	asrt.True(terr.Temporary())
}
