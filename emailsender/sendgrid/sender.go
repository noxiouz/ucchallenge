package sendgrid

import (
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"time"

	"github.com/mitchellh/mapstructure"
	"github.com/sendgrid/rest"
	sendgridapi "github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"

	"bitbucket.org/noxiouz/ucchallenge/emailsender"
)

const pluginName = "sendgrid"

func init() {
	emailsender.RegisterPlugin(pluginName, newSender)
}

const (
	v3mailSend             = "/v3/mail/send"
	defaultSendgridAddress = "https://api.sendgrid.com"
)

var (
	// ErrNoAPIKey means that `api_key` is missing in the configuration
	ErrNoAPIKey = errors.New("api_key is not specified")
)

type config struct {
	// APIKey is a secretkey to use Sendgrid api
	APIKey string `json:"api_key"`
	// Address is destination of Sendgrid. If it's empty "https://api.sendgrid.com" will be used
	Address string `json:"address"`

	// From described from which email all emails will be sent
	// It's formatted as User <email>
	From struct {
		User  string `json:"user"`
		Email string `json:"email"`
	} `json:"from"`
}

type sendgrid struct {
	config

	client rest.Client
	tr     *http.Transport
}

func decodeConfig(rawcfg map[string]interface{}, cfg *config) error {
	decoderConfig := mapstructure.DecoderConfig{
		WeaklyTypedInput: true,
		Result:           cfg,
		TagName:          "json",
	}

	decoder, err := mapstructure.NewDecoder(&decoderConfig)
	if err != nil {
		return err
	}

	if err = decoder.Decode(rawcfg); err != nil {
		return err
	}

	if cfg.APIKey == "" {
		return ErrNoAPIKey
	}

	if cfg.Address == "" {
		cfg.Address = defaultSendgridAddress
	}

	return nil
}

func newSender(rawcfg map[string]interface{}) (emailsender.Sender, error) {
	cfg := config{
		Address: defaultSendgridAddress,
	}

	if err := decodeConfig(rawcfg, &cfg); err != nil {
		return nil, err
	}

	tr := &http.Transport{
		DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
			d := net.Dialer{
				// I believe that IPv6 has come
				DualStack: true,
			}
			return d.DialContext(ctx, network, addr)
		},
		// probably it should be configurable
		MaxIdleConns: 10,
	}

	restClient := rest.Client{
		HTTPClient: &http.Client{
			Transport: tr,
		},
	}

	initMetricsOnce.Do(initMetrics)

	s := &sendgrid{
		config: cfg,
		client: restClient,
		tr:     tr,
	}

	return s, nil
}

func (s *sendgrid) Send(ctx context.Context, m emailsender.Mail) error {
	start := time.Now()

	from := mail.NewEmail(s.config.From.User, s.config.From.Email)
	to := mail.NewEmail(m.Name, m.Email)
	content := mail.NewContent("text/plain", m.Message)
	formedMail := mail.NewV3MailInit(from, m.Subject, to, content)
	requestBody := mail.GetRequestBody(formedMail)

	request := sendgridapi.GetRequest(s.config.APIKey, v3mailSend, s.config.Address)
	request.Method = "POST"
	request.Body = requestBody

	// NOTE: sendgrid API library does not allow to pass our Context to it
	for i := 2; i > 0; i-- {
		response, err := s.client.API(request)
		if err != nil {
			if i > 0 {
				continue
			}

			mailSentErrorTimer.UpdateSince(start)
			return err
		}
		switch response.StatusCode {
		case http.StatusOK:
			mailSentOkTimer.UpdateSince(start)
			return nil
		case http.StatusTooManyRequests:
			// TODO: we can return estimated limit reset somehow
			mailSentErrorTimer.UpdateSince(start)
			return emailsender.NewTemporaryError(fmt.Errorf("Too many requests"))
		case http.StatusUnauthorized:
			mailSentErrorTimer.UpdateSince(start)
			return fmt.Errorf("Unauthorized")
		}
	}
	return nil
}

func (s *sendgrid) Ping(ctx context.Context) error {
	// TODO: find a proper way to check it
	return nil
}

func (s *sendgrid) Close() {
	s.tr.CloseIdleConnections()
}
