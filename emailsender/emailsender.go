package emailsender

import (
	"context"
	"fmt"
	"math/rand"

	"github.com/uber-go/zap"

	"bitbucket.org/noxiouz/ucchallenge/utils/log"
)

// To describes destination point
type To struct {
	Name  string
	Email string
}

// Mail represents email destination, subject, message
type Mail struct {
	To
	Subject string
	Message string
}

// Sender is a general interface for email sending providers
type Sender interface {
	Send(ctx context.Context, m Mail) error
	Ping(ctx context.Context) error
	Close()
}

// Error represents Sender plugin error
type Error interface {
	error
	Temporary() bool
}

type temporaryError struct {
	error
}

func (temporaryError) Temporary() bool { return true }

func NewTemporaryError(err error) Error {
	return temporaryError{error: err}
}

type backendItem struct {
	s    Sender
	name string
}

// Balancer distributes emails across backends
type Balancer struct {
	backends []backendItem
}

// NewBalancer creates new Balancer without backends
func NewBalancer() *Balancer {
	return &Balancer{
		backends: make([]backendItem, 0),
	}
}

// Add attaches Sender to the Balancer
func (b *Balancer) Add(name string, s Sender) {
	b.backends = append(b.backends, backendItem{s: s, name: name})
}

// Close deallocates Senders
func (b *Balancer) Close() {
	for _, item := range b.backends {
		item.s.Close()
	}
}

// Send tries to send an email via all possible providers
// Send uses random perm to pick order of providers.
// The idea is based on the fact that amount of providers is about 4-5
// Each of them is very reliable, so probability of at least two of them is really small.
// More complex algorithm might count on dynamic weights, but should respect such factors
// as cost of email, rate limitation of evety service, and many other.
func (b *Balancer) Send(ctx context.Context, m Mail) error {
	perms := rand.Perm(len(b.backends))
	for _, index := range perms {
		err := b.backends[index].s.Send(ctx, m)
		if err == nil {
			return nil
		}
		log.G(ctx).Warn("email sending failed", zap.String("provider", b.backends[index].name), zap.Error(err))
	}
	log.G(ctx).Error("unable to send email via any of providers")
	return fmt.Errorf("unable to send email via any of providers")
}
