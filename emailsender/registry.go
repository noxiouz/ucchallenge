package emailsender

import (
	"errors"
	"sync"
)

// SenderPluginFactory is used to initialize new sender from config
type SenderPluginFactory func(config map[string]interface{}) (Sender, error)

var (
	registeredPlugins = map[string]SenderPluginFactory{}
	// although registration is not going to be a dynamic action
	// mu, protcting registeredPlugins can be useful in tests
	mu sync.Mutex

	// ErrPluginAlreadyRegistered returns when duplicted pluigns is being registering
	ErrPluginAlreadyRegistered = errors.New("plugin is already registered")
	// ErrNoPlugin means that a plugin with a given name has not been registered
	ErrNoPlugin = errors.New("no such plugin")
)

// RegisterPlugin sets factory for the name
func RegisterPlugin(name string, f SenderPluginFactory) error {
	mu.Lock()
	defer mu.Unlock()
	_, ok := registeredPlugins[name]
	if ok {
		return ErrPluginAlreadyRegistered
	}

	registeredPlugins[name] = f
	return nil
}

// NewSender creates new Sender, using SenderPluginFactory previously registered with name.
// It returns ErrNoPlugin if a requested plugin is not registered
func NewSender(name string, config map[string]interface{}) (Sender, error) {
	mu.Lock()
	defer mu.Unlock()
	f, ok := registeredPlugins[name]
	if !ok {
		return nil, ErrNoPlugin
	}

	// NewSender is not going to be a hot spot. No need to unlock mu before
	return f(config)
}
