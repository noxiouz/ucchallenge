package amazonses

import (
	"context"
	"errors"
	"time"

	"github.com/mitchellh/mapstructure"
	ses "github.com/sourcegraph/go-ses"

	"bitbucket.org/noxiouz/ucchallenge/emailsender"
)

const pluginName = "amazonses"

func init() {
	emailsender.RegisterPlugin(pluginName, newSender)
}

var (
	// ErrNoSecretAccessKey means `secretaccesskey` parameter is missing
	ErrNoSecretAccessKey = errors.New("AWS SecretAccessKey must be specified via `secretaccesskey`")
	// ErrNoAccessKeyID means `accesskeyid` parameter is missing
	ErrNoAccessKeyID = errors.New("AWS AccessKeyID must be specified via `accesskeyid`")
	// ErrNoEndpoint means `endpoint` parameter is missing
	ErrNoEndpoint = errors.New("AWS Endpoint must be specified via `endpoint`")
	// ErrNoVerifiedEmail means `verifiedemail` parameter is missing
	ErrNoVerifiedEmail = errors.New("AWS Verified Email must be specified via `verifiedemail`")
)

type config struct {
	// AccessKeyID is your Amazon AWS access key ID
	AccessKeyID string `json:"accesskeyid"`
	// Endpoint is the AWS endpoint to use for requests
	Endpoint string `json:"endpoint"`
	// SecretAccessKey is your Amazon AWS secret key
	SecretAccessKey string `json:"secretaccesskey"`
	// VerifiedEmail is your verifed email on AWS
	VerifiedEmail string `json:"verifiedemail"`
}

type sender struct {
	config
	ses.Config
}

func decodeConfig(rawcfg map[string]interface{}, cfg *config) error {
	decoderConfig := mapstructure.DecoderConfig{
		WeaklyTypedInput: true,
		Result:           cfg,
		TagName:          "json",
	}

	decoder, err := mapstructure.NewDecoder(&decoderConfig)
	if err != nil {
		return err
	}

	if err = decoder.Decode(rawcfg); err != nil {
		return err
	}

	// TODO: more strict check like that email is valid, Endpoint is parsed URL
	if cfg.AccessKeyID == "" {
		return ErrNoAccessKeyID
	}

	if cfg.Endpoint == "" {
		return ErrNoEndpoint
	}

	if cfg.SecretAccessKey == "" {
		return ErrNoSecretAccessKey
	}

	if cfg.VerifiedEmail == "" {
		return ErrNoVerifiedEmail
	}

	return nil
}

func newSender(rawcfg map[string]interface{}) (emailsender.Sender, error) {
	var cfg config

	if err := decodeConfig(rawcfg, &cfg); err != nil {
		return nil, err
	}

	s := sender{
		config: cfg,
		Config: ses.Config{
			AccessKeyID:     cfg.AccessKeyID,
			SecretAccessKey: cfg.SecretAccessKey,
			Endpoint:        cfg.Endpoint,
		},
	}

	// Initialize metrics only if this type of Sender is being used
	initMetricsOnce.Do(initMetrics)

	return &s, nil
}

func (s *sender) Send(ctx context.Context, m emailsender.Mail) error {
	// TODO: internally go-ses uses http.DefaultClient.
	// It prevents us from Transport tuning (IdleConnections, IPv6-friendly dialer, etc)
	// Try to find more convenient library
	start := time.Now()
	_, err := s.Config.SendEmail(s.config.VerifiedEmail, m.Email, m.Subject, m.Message)
	if err != nil {
		mailSentErrorTimer.UpdateSince(start)
		return err
	}
	mailSentOkTimer.UpdateSince(start)
	return nil
}

func (*sender) Close() {
	// go-ses uses http.DefaultClient. It is bad, but it's nothing to close here
}

func (s *sender) Ping(ctx context.Context) error {
	// TODO: find a proper way to check it
	return nil
}
