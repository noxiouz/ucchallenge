package amazonses

import (
	"context"
	"os"
	"testing"

	"bitbucket.org/noxiouz/ucchallenge/emailsender"

	"github.com/stretchr/testify/assert"
)

func TestConfigDecoder(t *testing.T) {
	asrt := assert.New(t)

	rawcfg := map[string]interface{}{}
	var cfg config

	err := decodeConfig(rawcfg, &cfg)
	asrt.EqualError(err, ErrNoAccessKeyID.Error())

	rawcfg["accesskeyid"] = "someaccesskeyid"
	err = decodeConfig(rawcfg, &cfg)
	asrt.EqualError(err, ErrNoEndpoint.Error())

	rawcfg["endpoint"] = "http://someamazone.com"
	err = decodeConfig(rawcfg, &cfg)
	asrt.EqualError(err, ErrNoSecretAccessKey.Error())

	rawcfg["secretaccesskey"] = "somesecretaccesskey"
	err = decodeConfig(rawcfg, &cfg)
	asrt.EqualError(err, ErrNoVerifiedEmail.Error())

	rawcfg["verifiedemail"] = "example@mail.com"
	err = decodeConfig(rawcfg, &cfg)
	asrt.NoError(err)
}

func TestSendEmail(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}
	asrt := assert.New(t)
	ctx := context.Background()

	rawcfg := map[string]interface{}{
		"accesskeyid":     os.Getenv("AWSAccessKeyId"),
		"secretaccesskey": os.Getenv("AWSSecretKey"),
		"verifiedemail":   os.Getenv("AWSVerifiedEmail"),
		"endpoint":        "https://email.us-west-2.amazonaws.com",
	}
	s, err := newSender(rawcfg)
	if !asrt.NoError(err) {
		t.Fatalf("unabale to create SES client %v", err)
	}
	defer s.Close()

	m := emailsender.Mail{
		Message: "test message from Noxiouz",
		Subject: "test message from noxiouz",
		To: emailsender.To{
			Name:  "noxiouz",
			Email: "noxiouz@yandex.ru",
		},
	}
	err = s.Send(ctx, m)
	asrt.NoError(err)
}
