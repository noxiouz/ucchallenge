package emailsender

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRegistry(t *testing.T) {
	asrt := assert.New(t)

	pluginName := "some_plugin_name"
	fakeError := fmt.Errorf("can not create pluign")
	fakePluginFactory := func(config map[string]interface{}) (Sender, error) {
		return nil, fakeError
	}
	config := make(map[string]interface{}, 1)

	s, err := NewSender(pluginName, config)
	asrt.EqualError(err, ErrNoPlugin.Error(), "returns non-nil error in case of missing plugin")
	asrt.Nil(s)

	asrt.NoError(RegisterPlugin(pluginName, fakePluginFactory), "error occured on a new pluign registration")
	asrt.EqualError(RegisterPlugin(pluginName, fakePluginFactory), ErrPluginAlreadyRegistered.Error(), "duplicate registration is not detected")

	s, err = NewSender(pluginName, config)
	asrt.Nil(s)
	asrt.EqualError(err, fakeError.Error(), "NewSender eats factory error")
}
