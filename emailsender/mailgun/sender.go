// +build mailgun

// As I have no access to Mailgun due to Business Verification process
// this plugin is not tested and not enabled by default

package mailgun

import (
	"context"
	"errors"
	"net"
	"net/http"

	"github.com/mitchellh/mapstructure"
	"gopkg.in/mailgun/mailgun-go.v1"

	"bitbucket.org/noxiouz/ucchallenge/emailsender"
)

const pluginName = "mailgun"

func init() {
	emailsender.RegisterPlugin(pluginName, newSender)
}

var (
	// ErrNoAPIKey means that `api_key` is missing in the configuration
	ErrNoAPIKey = errors.New("api_key is not specified")
	// ErrNoDomain means that `domain` is missing in the configuration
	ErrNoDomain = errors.New("domain is not specified")
)

type config struct {
	APIKey string `json:"api_key"`
	Domain string `json:"domain"`
}

type sender struct {
	config
	mailgun.Mailgun

	// This Transport is used by http.Client passed to Mailgun
	// When the sender closes, we close Idle connections from this Transport
	tr *http.Transport
}

func decodeConfig(rawcfg map[string]interface{}, cfg *config) error {
	decoderConfig := mapstructure.DecoderConfig{
		WeaklyTypedInput: true,
		Result:           cfg,
		TagName:          "json",
	}

	decoder, err := mapstructure.NewDecoder(&decoderConfig)
	if err != nil {
		return err
	}

	if err = decoder.Decode(rawcfg); err != nil {
		return err
	}

	if cfg.APIKey == "" {
		return ErrNoAPIKey
	}

	if cfg.Domain == "" {
		return ErrNoDomain
	}

	return nil
}

func newSender(rawcfg map[string]interface{}) (emailsender.Sender, error) {
	cfg := config{}

	if err := decodeConfig(rawcfg, &cfg); err != nil {
		return nil, err
	}

	tr := &http.Transport{
		DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
			d := net.Dialer{
				// I believe that IPv6 has come
				DualStack: true,
			}
			return d.DialContext(ctx, network, addr)
		},
		// probably it should be configurable
		MaxIdleConns: 10,
	}

	mg := mailgun.NewMailgun(cfg.Domain, cfg.APIKey, "")

	s := &sender{
		config:  cfg,
		Mailgun: mg,
		tr:      tr,
	}

	initMetricsOnce.Do(initMetrics)

	return s, nil
}

func (s *sender) Send(ctx context.Context, m emailsender.Mail) error {
	// NOTE: as I have no access to Mailgun because Business Verification
	// this is just an example
	message := mailgun.NewMessage(s.config.Domain, m.Subject, m.Message, m.Email)
	_, _, err := s.Mailgun.Send(message)
	if err != nil {
		return err
	}
	return nil
}

func (s *sender) Close() {
	s.tr.CloseIdleConnections()
}

func (s *sender) Ping(context.Context) error {
	// TODO: find a proper way to check it
	return nil
}
