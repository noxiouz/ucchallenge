// +build mailgun

package mailgun

import (
	"sync"

	"github.com/rcrowley/go-metrics"
)

var (
	// This timer measures time spent to send an email if no error occured
	mailSentOkTimer = metrics.NewTimer()
	// This time measures time spent to send an email in case of any error
	mailSentErrorTimer = metrics.NewTimer()
)

var initMetricsOnce sync.Once

func initMetrics() {
	registry := metrics.NewPrefixedChildRegistry(metrics.DefaultRegistry, pluginName)
	registry.Register("email_successfully_sent", mailSentOkTimer)
	registry.Register("email_badly_sent", mailSentErrorTimer)
}
